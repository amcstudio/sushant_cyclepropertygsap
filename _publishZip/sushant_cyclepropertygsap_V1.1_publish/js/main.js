'use strict';
~ function() {
    var easeInOut = Power2.easeInOut,
        ad = document.getElementById('mainContent');

    window.init = function() {
        createSquares();
        var tl = new TimelineMax();

        tl.set(ad, { force3D: false })

        tl.addLabel('frameOne')
        tl.staggerTo('.squareClass1', 1, { cycle:{
            y:function(a) {
                return a * 10},
 
            x:function(b) {
                return b * 10
            }
        }, yoyo:true, repeat:1,
        rotation: 0.01, ease: easeInOut },0.3,0);
// =========================
        tl.staggerTo('.squareClass2', 1, {
            cycle: {
                y: function (a) {
                    return a * -10
                },

                x: function (b) {
                    return b * -10
                }
        }, yoyo: true, repeat: 1,
        rotation: 0.01, ease: easeInOut }, 0.3,0);
// =========================
        tl.staggerTo('.squareClass3', 1, {
            cycle: {
                y: function (a) {
                    return a * -10
                },

                x: function (b) {
                    return b * 10
                }
        }, yoyo: true, repeat: 1,
        rotation: 0.01, ease: easeInOut }, 0.3,0);
// =========================
        tl.staggerTo('.squareClass4', 1, {
            cycle: {
                y: function (a) {
                    return a * 10
                },

                x: function (b) {
                    return b * -10
                }
        }, yoyo: true, repeat: 1,
        rotation: 0.01, ease: easeInOut}, 0.3,0);
        }

    function createSquares() {
        for (var i = 1; i <= 40; i++) {
            var square = document.createElement('div');
            square.id = 'redSquare' + i;
            if (i <= 10) {
                square.className = 'squareClass1';

            }
            if (i >= 10 && i <= 20) {
                square.className = 'squareClass2';

            }

            if (i >= 20 && i <= 30) {
                square.className = 'squareClass3';

            }
            if (i >= 30 && i <= 40) {
                square.className = 'squareClass4';
            }
            ad.appendChild(square);
        }
    }
}();
window.onload = init();